import { Injectable } from '@angular/core';

interface Datos{
  precio:number,
  codigo:string,
  email:string,
  nombre:string,
  apellido:string,
  empresa:string,
  direccion1:string,
  direccion2:string,
  pais:string,
  ciudad:string,
  provincia:string,
  cp:string,
  telefono:string,
  envio:any,
  pago:string,
  cardN:string,
  cardOwner:string,
  expM:string,
  expY:string,
  cvv:string
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public datos:Datos[]=[
{
  precio:119,
  codigo:"Codigo",
  email:"Email",
  nombre:"Nombre",
  apellido:"Apellido",
  empresa:"Empresa",
  direccion1:"Direccion1",
  direccion2:"Direccion2",
  pais:"Pais",
  ciudad:"Ciudad",
  provincia:"Provincia",
  cp:"Cp",
  telefono:"Telefono",
  envio:"Envio",
  pago:"Metodo pago",
  cardN:"Numero tarjeta",
  cardOwner:"Nombre tarjeta",
  expM:"Mes expiración",
  expY:"Año expiración",
  cvv:"CVV"
}
  ];

  constructor() { }
}
