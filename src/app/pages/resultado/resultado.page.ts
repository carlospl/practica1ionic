import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.page.html',
  styleUrls: ['./resultado.page.scss'],
})
export class ResultadoPage implements OnInit {

  constructor(public data:DataService,private router:Router,public alertController: AlertController) {
   }

  ngOnInit() {
 
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'DATOS GUARDADOS',
      message: 'Felicidades tus datos han sido guardados,su pedido esta en camino',
      buttons: ['OK']
    });

    await alert.present();
  }

  volverInicio(){
    this.router.navigate(['home']);
  }
  volver(){
    this.router.navigate(['segunda']);
  }
  
}
