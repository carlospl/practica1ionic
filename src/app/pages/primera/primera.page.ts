import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-primera',
  templateUrl: './primera.page.html',
  styleUrls: ['./primera.page.scss'],
})
export class PrimeraPage implements OnInit {
  envio:any;
  constructor(public data:DataService,private router:Router,public alertController: AlertController) { }

  ngOnInit() {
    
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      message: 'Tienes que seleccionar una de las dos opciones',
      buttons: ['OK']
    });

    await alert.present();
  }
  terceraPagina(){
    if(this.envio==10.58){
    this.data.datos[0].precio=parseInt(this.envio)+this.data.datos[0].precio;  
    this.data.datos[0].envio=this.envio;
    console.table(this.data.datos[0]);

  
    this.router.navigate(['segunda']);
    }
    if(this.envio==undefined){
      this.presentAlert();
    }else{
    this.data.datos[0].envio=this.envio;
    console.table(this.data.datos[0]);

  
    this.router.navigate(['segunda']);
    }
  }
  volver(){
    this.router.navigate(['home']);
  }
}
