import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-segunda',
  templateUrl: './segunda.page.html',
  styleUrls: ['./segunda.page.scss'],
})
export class SegundaPage implements OnInit {
direccionFinal:string;
pago:string;
cardN:string;
cardOwner:string;
cardMonth:string;
cardYear:string;
cvv:string;
  constructor(public data:DataService,private router:Router,public alertController: AlertController) {

   }
   async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      message: 'Tienes que seleccionar una de las dos opciones',
      buttons: ['OK']
    });

    await alert.present();
  }
  async alertDatos() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      message: 'Tienes que rellenar todos los datos',
      buttons: ['OK']
    });

    await alert.present();
  }
  ngOnInit() {
  }
  finalizar(){
   
    if(this.direccionFinal=="cambio"){
      if(this.data.datos[0].envio=="10.58"){
        this.data.datos[0].precio=this.data.datos[0].precio-parseInt(this.data.datos[0].envio); 
      }
        
      this.router.navigate(['home']);
    }if(this.direccionFinal==undefined){
        this.presentAlert();
    }
    if(this.direccionFinal=="igual"){
      if(this.pago==undefined){
        this.alertDatos()
      }
      if(this.pago!="Credito"){this.data.datos[0].pago=this.pago; this.router.navigate(['resultado']);}

      if(this.pago=="Credito"){
        if(this.cardN==undefined){
          this.alertDatos()
        }else if(this.cardOwner==undefined){
          this.alertDatos()
        }else if(this.cardMonth==undefined){
          this.alertDatos()
        }else if(this.cardYear==undefined){
          this.alertDatos()
        }else if(this.cvv==undefined){
          this.alertDatos()
        }else{
          this.data.datos[0].pago=this.pago;
          this.data.datos[0].cardN=this.cardN;
          this.data.datos[0].cardOwner=this.cardOwner;
          this.data.datos[0].expM=this.cardMonth;
          this.data.datos[0].expY=this.cardYear;
          this.data.datos[0].cvv=this.cvv;
          console.table(this.data.datos[0]);
          this.router.navigate(['resultado']);
        }
        
      }
      
    } 
  }
  volver(){
    this.router.navigate(['primera']);
  }

}
