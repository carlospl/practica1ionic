import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  precio:number;
  codigo:string;
  email:string;
  nombre:string;
  apellido:string;
  empresa:string;
  direccion1:string;
  direccion2:string;
  pais:string;
  ciudad:string;
  provincia:string;
  cp:string;
  telefono:string;
  constructor(public data:DataService,private router:Router,public alertController: AlertController) {}
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Alert',
      message: 'Rellena los campos faltantes',
      buttons: ['OK']
    });

    await alert.present();
  }
  segundaPantalla(){

    if(this.nombre==undefined){
      this.presentAlert();
 
    }
    else if(this.apellido==undefined){
      this.presentAlert();
 
    }
  
    else if(this.direccion1==undefined){
      this.presentAlert();

    }
    else if(this.direccion2==undefined){
      this.presentAlert();
    
    }
    else if(this.pais==undefined){
      this.presentAlert();

    }
    else if(this.ciudad==undefined){
      this.presentAlert();

    }
    else if(this.provincia==undefined){
      this.presentAlert();

    }
    else if(this.cp==undefined){
      this.presentAlert();

    }
    else if(this.telefono==undefined){
      this.presentAlert();
    }
    else if(this.email==undefined){
      this.presentAlert();

    }else{
      if(this.empresa==undefined){

      }else{
        this.data.datos[0].empresa=this.empresa;
      }
      this.data.datos[0].nombre=this.nombre;
      this.data.datos[0].apellido=this.apellido;
      this.data.datos[0].direccion1=this.direccion1;
      this.data.datos[0].direccion2=this.direccion2;
      this.data.datos[0].pais=this.pais;
      this.data.datos[0].ciudad=this.ciudad;
      this.data.datos[0].provincia=this.provincia;
      this.data.datos[0].cp=this.cp;
      this.data.datos[0].telefono=this.telefono;
      this.data.datos[0].email=this.email;
      console.table(this.data.datos[0]);
    this.router.navigate(['primera']);
    }
  
  }
  codigoAplicar(){
    this.data.datos[0].codigo=this.codigo;
    console.table(this.data.datos[0]);
    //this.codigo=this.data.datos[0].codigo;
    
  }

}


